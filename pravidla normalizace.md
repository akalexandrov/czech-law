# Náhrada jednotlivých symbolů:
```swift
"\u{9}" : "\u{20}", // horisontal tab -> space
"\u{a0}" : "\u{20}", // non-breaking space -> space
"\u{2028}" : "\u{a}", // line separator -> new line
"_" : "…", // low line -> ellipsis
"a\u{301}" : "á", "e\u{301}" : "é", "i\u{301}" : "í", "o\u{301}" : "ó", "u\u{301}" : "ú", "y\u{301}" : "ý", // czech specific combining diacritical marks
"A\u{301}" : "Á", "E\u{301}" : "É", "I\u{301}" : "Í", "O\u{301}" : "Ó", "U\u{301}" : "Ú", "Y\u{301}" : "Ý", // czech specific combining diacritical marks
"u\u{30a}" : "ů", "U\u{30a}" : "Ů", // czech specific combining diacritical marks
"c\u{30c}" : "č", "d\u{30c}" : "ď", "e\u{30c}" : "ě", "n\u{30c}" : "ň", "r\u{30c}" : "ř", "s\u{30c}" : "š", "t\u{30c}" : "ť", "z\u{30c}" : "ž", // czech specific combining diacritical marks
"C\u{30c}" : "Č", "D\u{30c}" : "Ď", "E\u{30c}" : "Ě", "N\u{30c}" : "Ň", "R\u{30c}" : "Ř", "S\u{30c}" : "Š", "T\u{30c}" : "Ť", "Z\u{30c}" : "Ž", // czech specific combining diacritical marks
"◄" : "", // markup characters from european cosolidated documents -> nothing
```

# Náhrada kombinací symbolů
```swift
"\n\n" : "\n", // double new line -> new line
"\n " : "\n", " \n" : "\n", // new line followed or preceded by space -> new line
"  " : " ", // double space -> single space
"\n- " : "\n— ", // hyphen in the beginning of line -> em dash
" - " : " — ", // hyphen between spaces -> em dash between spaces
"..." : "…", // triple dots -> ellipsis
" ." : ".", // space preceded by dot -> dot
" ;" : ";", // semicolon preceded by space -> semicolon
" ," : ",", // comma preceded by space -> comma
" :" : ":", // colon preceded and followed by space -> colon
"++" : "+", // double plus -> plus
"**" : "*", // double star -> star
"*\n" : "\n", // star at the end of line -> new line
"--" : "-", // double dash -> dash
"——" : "—", // double em dash -> em dash
"––" : "–", // double en dash -> em dash
"\n– " : "\n— ", // en dash -> em dash in the beginning of line
", — " : ", ", // em dash preceded by comma and space -> comma and space
" –\n" : "\n", // en dash preceded by space and followed by new line -> new line
"/ /" : "/", // slashes separated by space -> slash
"//" : "/", // double slashes -> slash
"( " : "(", "[ " : "[", // brackets followed by space -> brackets
" )" : ")", " ]" : "]", // brackets peceded by space -> brackets
")(" : ") (", // right and left parenthesis -> parenthesis separated by space
" “" : "“", // right double quotation mark preceded by space -> right double quotation mark
",“" : "“,", // right double quotation mark preceded by comma -> right double quotation mark followed by comma (eur-lex specific typo)
")§" : ") §", "]§" : "] §",  // section sign preceded by bracket => section sign separated by space
".§" : ". §", // section sign preceded by dot => section sign separated by space
"……" : "…", // double ellipsis -> ellipsis
"<=" : "≤", ">=" : "≥", // less than and equal, greater than and equal -> less than, greater than or equal
" m2" : " m²", // square meter
" m3" : " m³", // cubic meter
" cm3" : " cm³", // cubic centimetre
" /EU" : "/EU", // eur-lex specific typo
"č°" : "č.", // eur-lex specific typo in Official Journal EU No. 7, 30.1.1961
"Sb.*)" : "Sb. *)", // missed space after dot - specific kontext
"vkládají slova \", " : "vkládají slova „, ", // smart left quotation mark - specific kontext
"za slova \", " : "za slova „, ", // smart left quotation mark - specific kontext
```
# Regulární výrazy
```swift
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[1]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][1]"#, of: "1", with: " 1", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[2]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][2]"#, of: "2", with: " 2", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[3]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][3]"#, of: "3", with: " 3", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[4]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][4]"#, of: "4", with: " 4", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[5]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][5]"#, of: "5", with: " 5", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[6]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][6]"#, of: "6", with: " 6", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[7]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][7]"#, of: "7", with: " 7", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[8]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][8]"#, of: "8", with: " 8", silent: true), // missed space before footnote
#"\s[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[9]+[0-9a-z]*\)"# : (substr: #"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ][9]"#, of: "9", with: " 9", silent: true), // missed space before footnote
#"[►▼][A-Z][0-9]*\s"# : (substr: #"[►▼][A-Z][0-9]*\s"#, of: "", with: "", silent: true), // eur-lex specific markup
#"\s[►▼][A-Z][0-9]*,"# : (substr: #"\s[►▼][A-Z][0-9]*"#, of: "", with: "", silent: true), // eur-lex specific markup
#"(HLAVA|Kapitola|KAPITOLA|Oddíl|Pododdíl).+-\n"# : (substr: #" -\n"#, of: " -", with: "", silent: true), // eur-lex specific markup
#"\(\*[0-9]+\)"# : (substr: #"[0-9]+"#, of: "", with: "", silent: true), // eur-lex specific markup
#"\([0-9]+\)\n[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]"# : (substr: #"\n"#, of: "\n", with: " ", silent: true), // eur-lex specific markup
#"[a-z]{1,2}\)\n[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]"# : (substr: #"\n"#, of: "\n", with: " ", silent: true), // eur-lex specific markup
#"\(20[0-9]{2}\)20[0-9]{2}/"# : (substr: #"\)"#, of: ")", with: ") (", silent: true), // eur-lex specific typo
#"\([0-9]{1,2}\)[0-9]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // eur-lex specific typo
#"\+\)"# : (substr: #"\+"#, of: "+", with: "*", silent: true), // plus -> star
#"\*\)[a-záéíóúýůčďěňřšťž]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // missed space after right parenthesis
#"“\)[0-9]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // missed space after right parenthesis
#"[0-9]+\)[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // missed space after right parenthesis
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\)[0-9]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // missed space after right parenthesis
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\)[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #"\)"#, of: ")", with: ") ", silent: true), // missed space after right parenthesis
#"[0-9a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\*\)"# : (substr: #"\*"#, of: "*", with: " *", silent: true), // missed space before star
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+,[0-9]+\)"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+,\*\)"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+,[a-záéíóúýůčďěňřšťž]+"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"\.,[a-záéíóúýůčďěňřšťž]+"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"\),[0-9]+\)"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"\),[a-záéíóúýůčďěňřšťž]+"# : (substr: #","#, of: ",", with: ", ", silent: true), // missed space after comma
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+,?- [a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #",?- "#, of: "-", with: " —", silent: true), // missed space before hyphen
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+ -[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #" -"#, of: "-", with: "— ", silent: true), // missed space after hyphen
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\.\.\s"# : (substr: #"\.\."#, of: "..", with: ".", silent: true), // double dot
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\"[0-9]+"# : (substr: #"\""#, of: "\"", with: "\" ", silent: true), // missed space after quotation mark
#"[\s\(]\"[\(0-9a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #"\""#, of: "\"", with: "„", silent: true), // smart left quotation mark
#"[\)!\.,:\-0-9a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+\"[\s\)\.,]"# : (substr: #"\""#, of: "\"", with: "“", silent: true), // smart right quotation mark
#":[0-9a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+"# : (substr: #":"#, of: ":", with: ": ", silent: false), // colon followed by letter or digit
#";[0-9]+"# : (substr: #";"#, of: ";", with: "; ", silent: true), // semicolon followed by digit
#"[0-9a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+…"# : (substr: #"…"#, of: "…", with: " …", silent: true), // ellipsis preceded by letter or digit in živnostesnký zákon
#"\n—\n"#: (substr: #"—\n"#, of: "", with: "", silent: true), // improper use of em dash
#"[a-záéíóúýůčďěňřšťžA-ZÁÉÍÓÚÝŮČĎĚŇŘŠŤŽ]+[aelnouyáí]–li"#: (substr: #"–"#, of: "–", with: "-", silent: true),
#"\s[0-9]{1,3}(\ [0-9]{3})+\s"# : (substr: #"[0-9]{1,3}(\ [0-9]{3})+"#, of: "\u{20}", with: "", silent: true), // large numbers separated by spaces
```

# Dovolené kombinace symbolů
```swift
#"[%\),\-\.0Aa:;\]“…]\n[\(\*0Aa\[§„—]"#, // new line
#"[%'\)\*,\.0Aa:;=\]§½–—…≤≥“] [%\(\*0Aa=\[§–—„‚…≤≥]"#, // space
#"[a]![ “]"#, // exclamation mark
#"[Aa]'[ Aa]"#, // apostroph
#"[\s„]\([\*0Aa§„…]"#, // left parenthesis
#"[\)\*\.0Aa…“]\)[\s,\.;:\)\]“]"#, // right parenthesis
#"[Aa]![ ]"#, // exclamation mark
#"[\n \(]\*[\)]"#, // star
#"[ 0]\+[ 0]"#, // plus
#"[,0Aa]-[\s0Aa“]"#, // hyphen
#"[%\)\]0Aa“]\.[\s,;\)0Aa‘“]"#, // dot
#"[%\)\].0Aa“],[\s\-0“]"#, // comma
#"[0Aa]/[0Aa]"#, // slash
#"[ 0]%[\s,\.;]"#, // percent sign
#"[%\)\.0Aa\]“][:;][\s“]"#, // colon and semicolon
#" = "#, // equals sign
#"[\s]\[[0Aa\(§]"#, // left square bracket
#"[\)0Aa]\][\s\.,;“]"#, // right square bracket
#"[0][¼½¾]\s"#, // vulgar fractions
#"[\s\(\[]§ "#, // section sign
#"\s—[ 0Aa]"#, // em dash
#"[0 ]–[0 ]"#, // en dash
#"[\s\(]„[0Aa\(]"#, // left double quotation mark
#"[\)\.\-!0Aa:\]‘]“[\s\),\.:;]"#, // right double quotation mark
#"[\s\(“]‚[0Aa]"#, // left single quotation mark
#"[\)\.0Aa:\]]‘[\s\),\.:;“]"#, // right single quotation mark
#"[\s\(]…[\s\)]"#, // ellipsis
```
# Zakázané kombinace symbolů 
```swift
// single characters
#"\t"#, // horisontal tab
#"\x{a0}"#, // non-breaking space
#"_"#, // low line
#"’"#, // right single quotation mark
#"\x{301}"#, #"\x{30a}"#, #"\x{30c}"#,  // combining diacritical marks
#"\x{2028}"#, // line separator
#"►"#, #"▼"#, #"◄"#, // markup characters from european cosolidated documents
// two characters sequences
#"(<=|=>)"#, // equal sign
#"\*\*"#, // double stars
#"\+\+"#, // double pluses
#"\-\-"#, // double hyphens
#"//"#, // double slashes
#"——"#, // double em dashes
#"––"#, // double en dashes
// left neighbours
#"[\s–—\*]\n."#, // new line
#"[\s\(\[] ."#, // space
#"[„],."#, // comma
#"[\n]-."#, // hyphen
#"\.\.."#, // dot
#"[0Aa\)]\(."#, // left parenthesis
#"[\.\)]§."#, // section sign
#"[0Aa…]…."#, // ellipsis
#"[\s,]“."#, // right double quotation mark
#"[\s,]‘."#, // right single quotation mark
// right neighbours
#".\n[\s\.]"#, // new line
#". [\s!\)\],\.:;]"#, // space
#".\)[0Aa\(]"#, // right parenthesis
#".\*n\"#, // star
#".,[a\*]"#, // comma
#".[:;][0Aa]"#, // colon and semicolon
#".„[,]"#, // left double quotation mark
// full contextes
#" -[ 0Aa]"#, // hyphen between spaces of followed by letter or digit
#", —"#, // em dash preceded by comma and space
#"\* \*"#, // stars separated by space
#"/ /"#, // slashes separated by space
#"[Aa]–[Aa]"#, // en dash
```