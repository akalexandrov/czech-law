# Seznam právních předpisů

**AZ** Zákon č. 121/2000 Sb., o právu autorském, o právech souvisejících s právem autorským a o změně některých zákonů (autorský zákon)

**Brusel** Nařízení Evropského parlamentu a Rady (EU) č. 1215/2012 ze dne 12. prosince 2012 o příslušnosti a uznávání a výkonu soudních rozhodnutí v občanských a obchodních věcech (přepracované znění)

**DŘ** Zákon č. 280/2009 Sb., daňový řád

**EŘ** Zákon č. 120/2001 Sb., o soudních exekutorech a exekuční činnosti (exekuční řád) a o změně dalších zákonů

**GDPR** Nařízení Evropského parlamentu a Rady (EU) 2016/679 ze dne 27. dubna 2016 o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES (obecné nařízení o ochraně osobních údajů)

**IZ** Zákon č. 182/2006 Sb., o úpadku a způsobech jeho řešení (insolvenční zákon)

**KZ** Zákon č. 256/2013 Sb., o katastru nemovitostí (katastrální zákon)

**LZPEU** Listina základních práv Evropské unie

**LZPS** Usnesení č. 2/1993 Sb., předsednictva České národní rady o vyhlášení Listiny základních práv a svobod jako součástí ústavního pořádku České republiky

**NIns** Nařízení Evropského parlamentu a Rady (EU) 2015/848 ze dne 20. května 2015 o insolvenčním řízení (přepracované znění)

**NOZ** Zákon č. 89/2012 Sb., občanský zákoník

**NŘ** Zákon č. 358/1992 Sb., o notářích a jejich činnosti (notářský řád)

**OSŘ** Zákon č. 99/1963 Sb., občanský soudní řád

**Řím** Nařízení Evropského parlamentu a Rady (ES) č. 593/2008 ze dne 17. června 2008 o právu rozhodném pro smluvní závazkové vztahy (Řím I)

**SDPH** Směrnice Rady 2006/112/ES ze dne 28. listopadu 2006 o společném systému daně z přidané hodnoty

**SFEU** Smlouva o fungování Evropské unie

**SŘ** Zákon č. 500/2004 Sb., správní řád

**SŘS** Zákon č. 150/2002 Sb., soudní řád správní

**SZ** Zákon č. 183/2006 Sb., o územním plánování a stavebním řádu (stavební zákon)

**TŘ** Zákon č. 141/1961 Sb., o trestním řízení soudním (trestní řád)

**TZ** Zákon č. 40/2009 Sb., trestní zákoník

**ÚČR** Ústavní zákon č. 1/1993 Sb., Ústava České republiky

**ZDP** Zákon č. 586/1992 Sb., o daních z příjmů

**ZDPH** Zákon č. 235/2004 Sb., o dani z přidané hodnoty

**ZMPS** Zákon č. 91/2012 Sb., o mezinárodním právu soukromém

**ZOB** Zákon č. 128/2000 Sb., o obcích (obecní zřízení)

**ZOHS** Zákon č. 143/2001 Sb., o ochraně hospodářské soutěže a o změně některých zákonů (zákon o ochraně hospodářské soutěže)

**ZOK** Zákon č. 90/2012 Sb., o obchodních společnostech a družstvech (zákon o obchodních korporacích)

**ZOP** Zákon č. 250/2016 Sb., o odpovědnosti za přestupky a řízení o nich

**ZP** Zákon č. 262/2006 Sb., zákoník práce

**ZPC** Zákon č. 326/1999 Sb., o pobytu cizinců na území České republiky a o změně některých zákonů

**ZPrem** Zákon č. 125/2008 Sb., o přeměnách obchodních společností a družstev

**ZTOPO** Zákon č. 418/2011 Sb., o trestní odpovědnosti právnických osob a řízení proti nim

**ZÚS** Zákon č. 182/1993 Sb., o Ústavním soudu

**ZVR** Zákon č. 304/2013 Sb., o veřejných rejstřících právnických a fyzických osob

**ŽZ** Zákon č. 455/1991 Sb., o živnostenském podnikání (živnostenský zákon)

**ZZŘS** Zákon č. 292/2013 Sb., o zvláštních řízeních soudních

**ZZVZ** Zákon č. 134/2016 Sb., o zadávání veřejných zakázek
