# Zpracování předpisů

## 01 - Manuální zpracování

Odstranění:

- **odkazů na změny, novelizující předpisy**\
důvod: mají pouze technický význam, odkazy jsou vytvořeny automatický při tvorbě úplného znění,
- **obsah, přehled předpisu**\
důvod: má pouze technický a duplicitní charakter, 
- **zrušovací ustanovení** \
důvod: mohou odkazovat na názvy předpisů, které již nejsou relevantní, tedy nevypovídají o aktuálním stavu hlavního předpisu,
- **změny jiných předpisů**\
důvod: zasahují do textů jiných předpisů a tak nejsou součásti úplného znění hlavního předpisu a nejsou pro něj charakteristická,
- **přechodná ustanoveni**\
důvod: mohou odkazovat na skutečnosti, které přestaly být relevantními před aktuálním úplným zněním předpisu,
- **účinnost**\
důvod: má pouze technický význam.

Poznámky pod čárou jsou ponechány, protože obsahují odkazy na relevantní předpisy a slouží interpretačním účelům.

Přílohy jsou vetšinou ponechány když specifikuji předmětnou oblast předpisů. Výjimka je udělaná pro přílohy některých evropských předpisů, které obsahují pouze výčet pojmů v jiných úředních jazycích EU nebo odkazují na zrušené předpisy (převodové tabulky).      